package com.edu.springcloud.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * payment
 * @author 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public class Payment implements Serializable {
    /**
     * ID
     */
    @ApiModelProperty(value = "payment主键",example = "1")
    private Long id;

    @ApiModelProperty(value = "serial",example = "null")
    private String serial;

    private static final long serialVersionUID = -2015854452882929306L;
}
package com.edu.springcloud.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @作者 five-five
 * @创建时间 2021/6/6
 */
@RestController
@Api("OrderConsul相关接口(Consul作为注册服务中心)")
public class OrderConsulController {
    public static final String INVOKE_URL = "http://consul-provider-payment"; //consul-provider-payment

    @Autowired
    private RestTemplate restTemplate;

    @ApiOperation(value = "测试Consul作为注册服务中心的远程调用",notes = "测试Consul作为注册服务中心的远程调用")
    @GetMapping(value = "/consumer/payment/consul")
    public String paymentInfo() {
        //阿里云服务器中测试，会将服务视为不健康的。
        String result = restTemplate.getForObject(INVOKE_URL + "/payment/consul", String.class);
        System.out.println("消费者调用支付服务(consule)--->result:" + result);
        return result;
    }

}

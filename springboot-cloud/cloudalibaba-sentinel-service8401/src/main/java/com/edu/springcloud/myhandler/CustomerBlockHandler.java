package com.edu.springcloud.myhandler;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.edu.springcloud.entities.CommonResult;
import org.springframework.stereotype.Component;

/**
 * @作者 five-five
 * @创建时间 2021/6/12
 */
@Component
public class CustomerBlockHandler {
    public static CommonResult handleException(BlockException exception) {
        return new CommonResult(2021, "自定义的限流处理信息......CustomerBlockHandler");
    }

    public static CommonResult handleException2(BlockException exception) {
        return new CommonResult(2021, "自定义的限流处理信息......CustomerBlockHandler");
    }
}

package com.edu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @作者 five-five
 * @创建时间 2021/6/10
 */
@SpringBootApplication
@EnableDiscoveryClient
public class PaymentMain9992 {
    public static void main(String[] args) {
        SpringApplication.run(PaymentMain9992.class, args);
    }
}

package com.edu.springcloud.service.impl;

import com.edu.springcloud.service.IMessageProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

import java.util.UUID;

/**
 *
 * @作者 five-five
 * @创建时间 2021/6/9
 */
@EnableBinding(Source.class)//可以理解为一个消息的发送管道的定义
public class IMessageProviderImpl implements IMessageProvider {
    @Autowired
    private MessageChannel output;//消息的发送管道，结合SpringStream流程图来分析

    @Override
    public String send() {
        String serial = UUID.randomUUID().toString();
        Message<String> message = MessageBuilder.withPayload(serial).build();
        this.output.send(message); // 创建并发送消息
        System.out.println("***serial: " + serial);
        return serial;
    }
}

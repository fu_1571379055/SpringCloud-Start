package com.edu.springcloud.service;

/**
 * @作者 five-five
 * @创建时间 2021/6/9
 */
public interface IMessageProvider {
    String send();
}

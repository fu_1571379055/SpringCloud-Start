package com.edu.springcloud.service;

import com.edu.springcloud.entities.Payment;

/**
 * @作者 five-five
 * @创建时间 2021/6/4
 */
public interface PaymentService {
    int deleteByPrimaryKey(Long id);

    int insert(Payment record);

    int insertSelective(Payment record);

    Payment selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Payment record);

    int updateByPrimaryKey(Payment record);
}

package com.edu.springcloud.service.impl;

import com.edu.springcloud.dao.PaymentDao;
import com.edu.springcloud.entities.Payment;
import com.edu.springcloud.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @作者 five-five
 * @创建时间 2021/6/4
 */
@Service
public class PaymentImpl implements PaymentService {
    @Autowired
    private PaymentDao paymentDao;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return paymentDao.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Payment record) {
        return paymentDao.insert(record);
    }

    @Override
    public int insertSelective(Payment record) {
        return paymentDao.insertSelective(record);
    }

    @Override
    public Payment selectByPrimaryKey(Long id) {
        return paymentDao.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Payment record) {
        return paymentDao.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Payment record) {
        return paymentDao.updateByPrimaryKeySelective(record);
    }
}

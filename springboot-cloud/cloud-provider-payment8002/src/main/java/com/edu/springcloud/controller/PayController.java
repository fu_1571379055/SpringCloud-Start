package com.edu.springcloud.controller;


import com.edu.springcloud.entities.CommonResult;
import com.edu.springcloud.entities.Payment;
import com.edu.springcloud.service.PaymentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @作者 five-five
 * @创建时间 2021/6/4
 */
@RestController
@Slf4j
@Api(value = "Payment相关接口")
public class PayController {
    @Autowired
    private PaymentService paymentService;

    @Value(value = "${server.port}")//这行代码蕴含着yml文件读取后的保存格式
    private String serverport;

    @Autowired
    private DiscoveryClient discoveryClient;

    @PostMapping(value = "/payment/create")
    public CommonResult<Integer> create(
            @ApiParam(name = "payment", value = "payment")
                    Payment payment
    ) {
        int result = paymentService.insert(payment);
        log.info("插入结果。。。。。" + result);
        if (result > 0) {
            return new CommonResult<Integer>(200, "插入数据成功，serverport" + serverport, result);
        } else {
            return new CommonResult<>(444, "插入数据失败，serverport" + serverport);
        }
    }

    //下面的是必填字段，会显示require必填
    @ApiOperation(value = "根据payment的id进行查询", notes = "根据payment的id进行查询")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "主键", value = "id", defaultValue = "0")
//    })
    @GetMapping(value = "/payment/get/{id}")
    public CommonResult<Payment> get(@PathVariable("id") Long id) {
        Payment payment = paymentService.selectByPrimaryKey(id);
        if (payment != null) {
            return new CommonResult<Payment>(200, "查询成功，serverport" + serverport, payment);
        } else {
            return new CommonResult<>(444, "查询数据失败，serverport" + serverport);
        }
    }

    /**
     * 获取微服务的所有信息
     *
     * @return
     */
    @GetMapping(value = "/payment/discovery")
    public Object getDiscoveryInfo() {
        List<String> discoveryClientServices = discoveryClient.getServices();
        for (String discoveryClientService : discoveryClientServices) {
            log.info("----------------------" + discoveryClientService);
        }
        List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
        for (ServiceInstance instance : instances) {
            log.info(instance.getHost() + "\t" + instance.getInstanceId() + "\t" + instance.getUri());
        }
        return null;
    }

    /**
     * 测试自制LB(负载均衡)
     *
     * @return
     */
    @ApiOperation(value = "测试自制LB(负载均衡)", notes = "测试自制LB(负载均衡)")
    @GetMapping(value = "/payment/lb")
    public String getPaymentLB() {
        return serverport;
    }

    /**
     * 测试OpenFeign超时操作
     *
     * @return
     */
    @ApiOperation(value = "测试OpenFeign超时操作", notes = "测试OpenFeign超时操作")
    @GetMapping(value = "/payment/feign/timeout")
    public String paymentFeignTimeOut() {
        System.out.println("*****超时，paymentFeignTimeOut from port: " + serverport);
        //暂停几秒钟线程
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return serverport;
    }

    /**
     * 测试链路追踪器
     *
     * @return
     */
    @ApiOperation(value = "测试链路追踪器", notes = "测试链路追踪器")
    @GetMapping("/payment/zipkin")
    public String paymentZipkin() {
        return "hi ,i'am paymentzipkin server fall back，welcome to get five-five love，port=\t" + serverport + "\tO(∩_∩)O哈哈~";
    }


}

package com.edu.springcloud;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @作者 five-five
 * @创建时间 2021/6/4
 */
@SpringBootApplication
@EnableSwagger2//开启Swagger
@MapperScan(value = {"com.edu.springcloud.dao"})
@EnableEurekaClient//这个在新版自动有了，最新版springCloud就不需要了
public class PaymentMain8001 {
    public static void main(String[] args) {
        SpringApplication.run(PaymentMain8001.class, args);
    }
}

package com.edu.springcloud.controller;

import com.edu.springcloud.entities.CommonResult;
import com.edu.springcloud.entities.Payment;
import com.edu.springcloud.service.PaymentFeignService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @作者 five-five
 * @创建时间 2021/6/7
 */
@RestController
@Api("OpenFeign服务调用相关接口")
@Slf4j
public class OrderFeignController {
    @Autowired
    private PaymentFeignService paymentFeignService;

    /**
     * 测试OpenFeign调用接口消费
     * @param id
     * @return
     */
    @ApiOperation(value = "测试OpenFeign调用接口消费",notes = "测试OpenFeign调用接口消费    ")
    @GetMapping(value = "/consumer/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id) {
        CommonResult<Payment> paymentById = paymentFeignService.getPaymentById(id);
        log.info("=============="+paymentById+"===========");
        return paymentById;
    }

    /**
     * 测试OpenFeign超时操作
     *
     * @return
     */
    @ApiOperation(value = "测试OpenFeign超时操作", notes = "测试OpenFeign超时操作")
    @GetMapping(value = "/consumer/payment/feign/timeout")
    public String paymentFeignTimeOut() {
        //OpenFeign客户端一般默认等待1秒
        String s = paymentFeignService.paymentFeignTimeOut();
        return s;
    }


}

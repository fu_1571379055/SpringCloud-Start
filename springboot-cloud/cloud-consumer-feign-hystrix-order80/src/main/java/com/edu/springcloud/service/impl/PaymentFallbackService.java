package com.edu.springcloud.service.impl;

import com.edu.springcloud.service.PaymentHystrixService;
import org.springframework.stereotype.Component;

/**
 * @作者 five-five
 * @创建时间 2021/6/7
 */
@Component
public class PaymentFallbackService implements PaymentHystrixService {
    @Override
    public String paymentInfo_OK(Integer id) {
        return "---PaymentFallbackService fall back-paymentInfo_OK,+_1122211_+";
    }

    @Override
    public String paymentInfo_TimeOut(Integer id) {
        return "---PaymentFallbackService fall back-paymentInfo_TimeOut,+_1122211_+";
    }
}

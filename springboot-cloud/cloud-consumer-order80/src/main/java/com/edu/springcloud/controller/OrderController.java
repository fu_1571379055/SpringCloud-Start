package com.edu.springcloud.controller;

import com.edu.springcloud.entities.CommonResult;
import com.edu.springcloud.entities.Payment;
import com.edu.springcloud.lb.LoadBalance;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

/**
 * @作者 five-five
 * @创建时间 2021/6/4
 */
@RestController
@Slf4j
@Api(value = "订单消费者")
public class OrderController {
    //    public static final String PaymentSrv_URL = "http://localhost:8001";
    public static final String PaymentSrv_URL = "http://CLOUD-PAYMENT-SERVICE";

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private LoadBalance loadBalance;

    @Autowired
    private DiscoveryClient discoveryClient;

    @GetMapping("/consumer/payment/create") //客户端用浏览器是get请求，但是底层实质发送post调用服务端8001

    public CommonResult create(
            @ApiParam(name = "payment", value = "payment")
                    Payment payment) {
        return restTemplate.postForObject(PaymentSrv_URL + "/payment/create", payment, CommonResult.class);
    }

    /**
     * 返回对象为响应体中数据转化成的对象，基本上可以理解为Json
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "通过ID查询，调用的是RestTemplate.getForObject()", notes = "通过ID查询，调用的是RestTemplate.getForObject()")
    @GetMapping("/consumer/payment/get/{id}")
    public CommonResult getPayment(@PathVariable Long id) {
        return restTemplate.getForObject(PaymentSrv_URL + "/payment/get/" + id, CommonResult.class, id);
    }

    /**
     * 返回对象为ResponseEntity对象，包含了响应中的一些重要信息，比如响应头、响应状态码、响应体等
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "通过ID查询，调用的是RestTemplate.getForEntity()", notes = "通过ID查询，调用的是RestTemplate.getForEntity()")
    @GetMapping("/consumer/payment/getEntity/{id}")
    public CommonResult getPaymentToEntity(@PathVariable Long id) {
        ResponseEntity<CommonResult> entity = restTemplate.getForEntity(PaymentSrv_URL + "/payment/get/" + id, CommonResult.class, id);
        log.info(entity.getStatusCode() + "\t" + entity.getStatusCodeValue());
        if (entity.getStatusCode().is2xxSuccessful()) {
            return entity.getBody();
        }
        return new CommonResult<String>(444, "操作失败");
    }

    @ApiOperation(value = "调用我自己写的轮询算法实现类", notes = "调用我自己写的轮询算法实现类")
    @GetMapping(value = "/payment/lb")
    public String getPaymentLB() {
        List<ServiceInstance> serviceInstances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
        if (serviceInstances == null || serviceInstances.size() == 0) {
            return null;
        }
        ServiceInstance instance = loadBalance.instance(serviceInstances);
        URI uri = instance.getUri();
        return restTemplate.getForObject(uri + "/payment/lb", String.class);
    }

    /**
     * 测试链路追踪
     * @return
     */
    @ApiOperation(value = "测试链路追踪",notes = "测试链路追踪")
    @GetMapping("/consumer/payment/zipkin")
    public String paymentZipkin() {
        String result = restTemplate.getForObject(PaymentSrv_URL + "/payment/zipkin/", String.class);
        return result;
    }

}

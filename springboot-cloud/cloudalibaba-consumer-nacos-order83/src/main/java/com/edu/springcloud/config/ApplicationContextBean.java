package com.edu.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @作者 five-five
 * @创建时间 2021/6/10
 */
@Configuration
public class ApplicationContextBean {
    @Bean
    @LoadBalanced//Nacos的场景启动器默认集成了Netflix的ribbon依赖
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}

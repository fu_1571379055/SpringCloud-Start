package com.edu.springcloud.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;

/**
 * @作者 five-five
 * @创建时间 2021/6/4
 */
@Configuration
public class SwaggerConfig {
    /**
     * 整合Swagger文档
     *
     * @return
     */
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)//配置了Swagger的Docket的bean实例
                .pathMapping("/")
                .enable(true)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.edu.springcloud.controller"))//配置扫描那个包
                .paths(PathSelectors.any())
                .build().apiInfo(new ApiInfoBuilder()
                        .title("欢迎来到Swagger调试界面")
                        .description("SpringCloud项目起步，详细信息......")
                        .version("1.0")
                        .contact(new Contact("fivefive", "http://www.cnblogs.com/five-five", "1571379055@qq.com"))
                        .license("The Apache License")
                        .licenseUrl("http://www.cnblogs.com/five-five")
                        .termsOfServiceUrl("fivefive的散人协会")
                        .extensions(new ArrayList<>())
                        .build());
    }
}

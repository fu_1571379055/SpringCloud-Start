package com.edu.springcloud.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @作者 five-five
 * @创建时间 2021/6/6
 */
@RestController
@Api(value = "payment8004(Zookeeper作为注册中心)")
public class PaymentController {
    @Value("${server.port}")
    private String serverPort;

    @ApiOperation(value = "测试zookeeper注册进来没",notes = "测试zookeeper注册进来没")
    @RequestMapping(value = "/payment/zk",method = RequestMethod.GET)
    public String paymentzk() {
        return "springcloud with zookeeper: " + serverPort + "\t" + UUID.randomUUID().toString();
    }
}

package com.edu.springcloud.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @作者 five-five
 * @创建时间 2021/6/6
 */
@Slf4j
@Api("Consul服务提供者相关接口")
@RestController
public class PaymentController {
    @Value("${server.port}")
    private String serverPort;

    @ApiOperation(value = "Consul作为注册中心",notes = "Consul作为注册中心")
    @GetMapping("/payment/consul")
    public String paymentInfo() {
        return "springcloud with consul: " + serverPort + "\t\t" + UUID.randomUUID().toString();
    }

}

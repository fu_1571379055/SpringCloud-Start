package com.edu.springcloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @作者 five-five
 * @创建时间 2021/6/10
 */
@RestController
@RefreshScope   //在控制器类加入@RefreshScope注解使当前类下的配置支持Nacos的动态刷新功能。
public class ConfigClientController {
    @Value("${config.info}")
    private String configInfo;
    @Value("${config.info2}")
    private String configInfo2;

    @GetMapping("/config/info")
    public String getConfigInfo() {
        //测试了一波，云yml会覆盖application.yml的配置
        return configInfo+"\t"+configInfo2;
    }
}
